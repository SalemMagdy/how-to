import { Component, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from './auth/login/login.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'how-to';
    constructor(
        public dialog: MatDialog
    ) {

    }
    login() {
        const dialogRef = this.dialog.open(LoginComponent, {
            width: '450px',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
    signup() {
        const dialogRef = this.dialog.open(SignUpComponent, {
            width: '450px',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
    @HostListener("document:click", ["$event"])
    onDocumentClicked(ev) {
        if (ev.target.classList.contains('options-menu')) {
            ev.target.classList.toggle('active');
            if (ev.target.parentNode.classList.contains('active')) {
                this.fitMenu(ev.target);
            }
        }
        if (ev.target.closest('.options-menu')) {
            if (ev.target.parentNode.classList.contains('options-menu')) {
                ev.target.parentNode.classList.toggle('active');
                if (ev.target.parentNode.classList.contains('active')) {
                    this.fitMenu(ev.target.closest('.options-menu'));
                }
            }
        }
        let activeMenus = document.querySelectorAll('.options-menu.active');
        for (let i = 0; i < activeMenus.length; i++) {
            if (
                !(
                    ev.target.closest('.notifications-menu') === activeMenus[i]
                    || (
                        ev.target.closest('.options-menu') === activeMenus[i]
                        && !ev.target.closest('.options-menu-container')
                    )
                    || (ev.target === activeMenus[i])
                    || (ev.target.closest('.options-menu') === activeMenus[i] && ev.target.closest('.prevent'))
                )
                || ev.target.closest('.close-notifications-menu')
                || ev.target.classList.contains('close-notifications-menu')
            ) {
                activeMenus[i].classList.remove('active');
            }
        }
        if (ev.target.closest('.checkbox-with-child .checkbox-arrow')) {
            let checkboxArrow = ev.target.closest('.checkbox-with-child .checkbox-arrow');
            let checkboxParentContainer = checkboxArrow.closest('.checkbox-with-child');
            let children = checkboxParentContainer.querySelectorAll('.checkbox-with-child');
            if (checkboxParentContainer.classList.contains('active')) {
                for (let i = 0; i < children.length; i++) {
                    children[i].classList.remove('active')
                }
            }
            checkboxParentContainer.classList.toggle('active');
            if (checkboxParentContainer.classList.contains('active')) {
                console.log(checkboxParentContainer.offsetHeight, checkboxParentContainer.offsetTop)
            }
        }
        // .
        // .
        // skolera tabs 
        if (ev.target.closest('.skolera-tabs .navigation-links') && ev.target.classList.contains('single-link') || ev.target.classList.contains('skolera-tabs-link') || ev.target.closest('.skolera-tabs-link')) {
            ev.preventDefault();
            ev.stopPropagation();
        }
    }
    fitMenu(menu) {
        menu.classList.remove('reverse-v');
        menu.querySelectorAll('.options-menu-container')[0].style.maxHeight = 'none';
        menu.classList.remove('reverse-h');
        let offsetAndHeight = menu.querySelectorAll('.options-menu-container')[0].offsetHeight + menu.offsetTop;
        let widnowHeight = document.body.clientHeight;
        let outOfWindowVertically = offsetAndHeight - widnowHeight > 0 ? true : false;
        if (outOfWindowVertically) {
            menu.classList.add('reverse-v')
            menu.querySelectorAll('.options-menu-container')[0].style.maxHeight = (menu.offsetTop - 20) + 'px';
        }
    }
}
