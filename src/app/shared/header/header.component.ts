import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../../auth/login/login.component';
import { SignUpComponent } from '../../auth/sign-up/sign-up.component';
import { Observable, Subject, of, concat } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, delay } from 'rxjs/operators';
import { NotificationsComponent } from '../notifications/notifications.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    items:any = [1,2,3,4]
    constructor(
        public dialog: MatDialog
    ) {

    }
    ngOnInit() {
        this.loadPeople();
    }
    login() {
        const dialogRef = this.dialog.open(LoginComponent, {
            width: '450px',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
    signup() {
        const dialogRef = this.dialog.open(SignUpComponent, {
            width: '450px',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
    openAllNotifications(){
        const dialogRef = this.dialog.open(NotificationsComponent, {
            width: '500px',
            data: {}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
    onSearchClose() {
        document.body.classList.remove('search-focus');
    }
    onSearchFocus() {
        document.body.classList.add('search-focus');
    }

    people$: Observable<any[]>;
    peopleLoading = false;
    peopleInput$ = new Subject<string>();
    selectedPersons;
    private loadPeople() {
        this.people$ = concat(
            of([]), // default items
            this.peopleInput$.pipe(
                debounceTime(200),
                distinctUntilChanged(),
                tap(() => this.peopleLoading = true),
                switchMap(term => this.getPeople(term).pipe(
                    catchError(() => of([])), // empty list on error
                    tap(() => this.peopleLoading = false)
                ))
            )
        );
    }
    getPeople(term: string = null): Observable<any[]> {
        let items = getMockPeople();
        if (term) {
            items = items.filter(x => x.name.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1);
        }
        return of(items).pipe(delay(1000));
    }
    notificationsOpen = false;
    openNotifications() {
        this.notificationsOpen = !this.notificationsOpen;
    }

}

function getMockPeople() {
    return [
        {
            id: '5a15b13c36e7a7f00cf0d7cb',
            tag: 'Courses',
            name: '33 ways to cook the perfect chicken',
        },
        {
            id: '5a15b13c36e7da7f00cf0d7cb',
            tag: 'Topics',
            name: 'Chicken Recipes',
        },
        {
            id: '5a15b13c36e7a7f00cf0d7cb',
            tag: 'People',
            name: 'Guy Loves Chicken',
        },
        
    ]
}