import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { AuthModule } from '../auth/auth.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NotificationsComponent } from './notifications/notifications.component';

@NgModule({
  declarations: [HeaderComponent, SidebarComponent, NotificationsComponent],
  imports: [
    CommonModule,
    AuthModule,
    NgSelectModule,
    FormsModule
  ],
  exports: [
      HeaderComponent,
      SidebarComponent,
      NgSelectModule,
      FormsModule
    ],
    entryComponents: [NotificationsComponent]
})
export class SharedModule { }
