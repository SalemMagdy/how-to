import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@NgModule({
    declarations: [
        LoginComponent,
        SignUpComponent,
        ResetPasswordComponent
    ],
    exports: [
        LoginComponent,
        SignUpComponent,
        ResetPasswordComponent
    ],
    imports: [
        CommonModule,
        AuthRoutingModule
    ],
    entryComponents: [LoginComponent, SignUpComponent, ResetPasswordComponent]
})
export class AuthModule { }