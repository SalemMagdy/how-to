import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { SearchModule } from './search/search.module';

const routes: Routes = [
  {
  path: '', redirectTo: 'home', pathMatch: 'full'
},
{
  path: 'home', loadChildren: './search/search.module#SearchModule'
},

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
